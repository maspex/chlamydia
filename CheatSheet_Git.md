# **Cheatsheet Git** 

**Recuperer l'avancement du master:** 

- git pull master origin

**savoir sur quelle branche on est:**

- git branch

**Changer de branche :**

git checkout nom_de_la_branche

**Créer une branche :**

- git branche nom_de_ma_branche

**Récuperer le contenu de la branche ou l'on est (hors master):**

- git pull 

**envoyer dans le depot** 

- git push 

**Pour créer un raccourci du "push" vers sa branche :**

git push --set-upstream origin nomdemabranche

----------------------------------------------------

***pour merger  vers le master, aller dans l'interface git lab et faire une demande de merge.***


**Commande basic**

**voir le contenu d'un dossier :**

- ls (liste)

**Se deplacer dans l'arborecence de fichier :**

- cd 

**Modifier un fichier:** 

- nano
- gedit

**Devenir super-user *(attention au rm 
sudo)***

**Supprimer un fichier ou dossier ou ensemble de fichier/dossier:**

- rm nom_du_ficher

**creer un dossier/directory:**

- mkdir nom_du_dossier

**creer un fichier :**

touch nom_du_fichier


**protocole d'envoi :**

- git status
- git add .
- git commit -m "commentaire"
- git push 

