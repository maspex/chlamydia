"use strict";
let bouton_couleur;
let main;
let color_picker;
let btn_choix_color;
let btn_close_choix_color;
let context;
let canvas;
let index;
let context_pix;
let color_pix;
let rouge;
let bleu;
let vert;
let index_1;
let index_2;
let position_souris_x;
let position_souris_y;

window.addEventListener("DOMContentLoaded", (event) => {
    bouton_couleur = document.getElementsByClassName('bouton_changement');
    btn_choix_color= document.getElementsByClassName('ls_button_choix_color');
    btn_close_choix_color = document.getElementsByClassName('ld_close_color_picker');

    color_picker = document.getElementsByClassName('ld_div_color_picker');
    main = document.getElementsByClassName('color_main');

    canvas = document.getElementsByClassName('ld_div_degrader_color');
    if(canvas[0] !== undefined){
        context = canvas[0].getContext('2d');

    }


    for( index= 0; index < bouton_couleur.length ; index++){
        bouton_couleur[index].addEventListener('click', couleur);
        btn_choix_color[index].addEventListener('click', ouvrir_color_picker);
        btn_close_choix_color[index].addEventListener('click', close_color_picker);
        color_picker[index].addEventListener('click', couleur_pixel);
    }
    canvas_degrader(context);
});

function ouvrir_color_picker(){
    color_picker[0].classList.toggle('ld_hidden');
}

function close_color_picker(){
    color_picker[0].classList.add('ld_hidden');
}

function couleur_pixel(event){
    context_pix = context.getImageData(position_x(event), position_y(event), 1 , 1);
    color_pix = context_pix.data;
    changer_fond(color_pix);
}

function changer_fond(color_pix){
    rouge = color_pix[0];
    bleu = color_pix[1];
    vert = color_pix[2];
    main[0].style.backgroundColor = "rgb("+rouge+","+bleu+","+vert+")";
}

function canvas_degrader(context){
    for (index_1 = 0; index_1 < 6; index_1++) {
        for ( index_2 = 0; index_2 < 6; index_2++) {
            context.fillStyle = 'rgb(' + Math.floor(255 - 42.5 * index_1) + ',' +
                Math.floor(255 - 42.5 * index_2) + ',0)';
            context.fillRect(index_2 * 25, index_1 * 25, 25, 25);
        }
    }
}

function position_x(event) {
    position_souris_x = event.clientX;
    return position_souris_x;
}

function position_y(event) {
    position_souris_y = event.clientY;
    return position_souris_y;
}

function couleur(){
    if(main[0].classList.contains('ld_color_white')){
        main[0].style.backgroundColor = "rgb(0,0,0)";
        main[0].classList.remove('ld_color_white');
    }else {
        main[0].style.backgroundColor = "rgb(255,255,250)";
        main[0].classList.remove('ld_color_black');
    }
}