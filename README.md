# **Organisation projet Chlamydia**
-----------------------------------
# ** test du site ** 
Bonjours voici la marche suivre pour paramétrer le sites web

-mettre le site web sur votre bureau

-dans le terminale aller au le dossier “db” :

cd Bureau/chlamydia/db

-crée database ici avec le fichier database.sql :

sqlite3 database.db < database.sql

-fait un cd ../ pour retourner vers Bureau/chlamydia/

-lancer dans le terminal un localhost:

php -S localhost:8090

-dans le navigateur taper cette adresse:

http://localhost:8090/controller/accueil_controller.php

bonne visite merci !!


## Définition du projet : 

Création d'un site internet pour un restaurant, nommé Chlamydia. 
Il se composera de quatre pages : 
Actualités, produits, équipe et contact.

**Membre du groupe :** 

- Marius 
- Michaël
- Sarah 
- Loïc


**Rôles et responsabilités :**

Sarah Lauren -> master
Michaël, Marius et Loïc -> développeur.


**Git :**

Travail avec une branche master, plus cinq branches.
une branche par page et une branche pour les assets.


## Arborescence:
app 
 
    pages
        actualite.html
        produit.html
        equipe.html
        contact.html
        layout.html
    css
    img
index.php


Convention nommage des variables : snakeCase
Convention nommage des fichiers : minuscules


**Fonctionnement CSS :**

- Css génerique (un pour tous). 

**Codification des classes et id** 
- Chaque id ou classe sera nommée par les initiales du développeur de la page courante. 
    
    *Exemple : Nom du dev = Loic Damiano id = "#LD_title", class =".ld_title"*

## Code couleur du design : 
 - Or + Noir et blanc + nuances de gris 

