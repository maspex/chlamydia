<?php

// the dishes
$asperge=($_POST["mcasperge01"]);
$cochon=($_POST["mccochon01"]);
$agneau=($_POST["mcagneau01"]);
$cerise=($_POST["mccerise01"]);

// the prices
$tarifas = 70;
$tarifco = 80;
$tarifag = 90;
$tarifce = 35;

// number dishes
$totalas = $asperge * $tarifas;
$totalco = $cochon * $tarifco;
$totalag = $agneau * $tarifag;
$totalce = $cerise * $tarifce;

// total
$total = $totalas + $totalco + $totalag + $totalce ;

// tva 
$tva = $total * 0.055 ;
$tva = number_format($tva,2,',', '');

//connect db
class actuController  {

    private $pdo; 
    
    public function __construct(){
        try {
            $this->pdo = new PDO('sqlite:../db/database.db');
        } catch (PDOException $ex) {
            die("erreur ! ");
        }
        
    }
    
    public function getPDO() {
       
       return $this->pdo;
    
    }
    
    public function recordcmde($statement,$datas) {
       $pdo = $this->getPDO();
       $req = $pdo->prepare($statement)->execute($datas);
    }
    
    }

//push db
$d = [(int)$asperge,
      (int)$cochon,
      (int)$agneau,
      (int)$cerise,
      (int)$total];
$bdd = new actuController();

//( , '$asperge', '$chochon', '$agneau', '$cerise', '$total')
$req = $bdd->recordcmde("INSERT INTO commande(asperge, cochon, agneau, cerise, total) VALUES(?, ?, ?, ?, ?)",$d);

include "../pages/produit_result.phtml";
